#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import yaml
import locale
import twitter

from datetime import datetime
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Document, Date, Keyword, Text


class Tweet(Document):
    """
    Tweet representation for ES indices
    """
    author = Text(fields={'raw': Keyword()})
    content = Text(analyzer='snowball')
    author_id = Keyword()
    created = Date()

    class Index:
        name = 'tweets'

    def save(self, **kwargs):
        return super().save(**kwargs)


def publish_tweets(es_client, tweets):
    """
    Publishes the given list of tweets to elastic search
    :param es_client: The elastic search client
    :param tweets: The list of tweets as returned by the API client
    """
    print("\nWriting tweets to index")
    for t in tweets:
        doc = Tweet(author=t.user.screen_name, content=t.text, author_id=t.user.id)
        doc.meta.id = t.id
        doc.created = datetime.strptime(t.created_at, "%a %b %d %H:%M:%S %z %Y")
        doc.save(using=es_client)
        print(doc)


class Follower(Document):
    """
    Follower representation for ES indices,
    """
    following = Keyword()
    follower = Text(fields={'raw': Keyword()})
    follower_id = Keyword()

    class Index:
        name = 'followers'

    def save(self, **kwargs):
        return super().save(**kwargs)


def publish_followers(es_client, user, followers):
    """
    Publishes followers to elasticsearch
    :param es_client: The elastic search client
    :param user: The followed user
    :param followers: The list of followers as returned by the API client
    """
    print("\nWriting followers to index")
    for f in followers:
        doc = Follower(following=user.id, follower=f.screen_name, follower_id=f.id)
        doc.meta.id = "%d-%d" % (user.id, f.id)
        doc.save(using=es_client)
        print(doc)


def create_es(params):
    client = Elasticsearch(hosts=[params])
    return client


def create_twitter(auth_keys):
    return twitter.Api(auth_keys["consumer_key"], auth_keys["consumer_secret"], auth_keys["access_token_key"],
                       auth_keys["access_token_secret"], sleep_on_rate_limit=True)


def read_config():
    """
    Reads the YAML configuration file from working directory.
    Filename is config.yml
    """
    with open("config.yml", "r") as ymlfile:
        cfg = yaml.load(ymlfile)

    if "elastic" not in cfg or "host" not in cfg["elastic"] or not cfg["elastic"]["host"]:
        raise ValueError("Invalid elasticsearch configuration")

    if "twitter" not in cfg:  # Invalid tokens will yield an auth error later
        raise ValueError("Invalid Twitter API configuration")

    return cfg


if __name__ == "__main__":
    cfg = read_config()
    locale.setlocale(locale.LC_ALL, "en_US.UTF-8")  # Locales might OS dependent

    if len(sys.argv) < 2:
        print("No screen name specified, defaulting to realdonaldtrump")

    # Init es client and indices
    es_client = create_es(cfg["elastic"])
    Tweet.init(using=es_client)
    Follower.init(using=es_client)

    # Init twitter api
    twt_api = create_twitter(cfg["twitter"])
    screen_name = sys.argv[1] if len(sys.argv) > 1 else "realdonaldtrump"

    # Fetch last 5 tweets in the timeline
    last_tweets = twt_api.GetUserTimeline(screen_name=screen_name, count=5)
    user = last_tweets[0].user if last_tweets else twt_api.GetUser(screen_name=screen_name)

    # Push tweets to ES
    publish_tweets(es_client, last_tweets)

    # We limit followers to 50, otherwise because of rate limitations it will fail for people with many followers
    # TODO: add pagination
    followers = twt_api.GetFollowers(user.id, total_count=50, skip_status=True, include_user_entities=False)

    # Push followers to ES
    publish_followers(es_client, user, followers)
