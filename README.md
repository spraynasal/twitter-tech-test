# README #

This program reads five tweets from a given user and its list of followers (limited to 50 followers)

### Requirements ###

* Python 3+
* A running instance of elastic search 7.6+, a docker-compose file is provided in the docker subfolder
* The python libraries listed in `requirements.txt`

### Running the script ###

* Make a copy named `config.yml` of the sample config file, fill in the elastic search host and port, and the Twitter Dev API keys
* Run the `fetcher.py` script from the command line, the twitter user name is expected as a CLI parameter i.e. `fetcher.py username`
* If no user name is passed, it defaults to `realdonaldtrump`

### Seeing the results ###

* During running, the script will print IDs of **NEW** inserted documents on the shell, you can use those to see documents using the standard elastic search API.

To get a tweet:
`curl -XGET "localhost:9200/tweets/_doc/<id>?pretty"`

To get a follower:
`curl -XGET "localhost:9200/followers/_doc/<id>?pretty"`

